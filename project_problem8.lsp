;;;**************************************************************************************;;;
;;;                                       Περιγραφή                                      ;;;
;;;                                                                                      ;;;
;;; Το αρχείο αυτό περιέχει το "μενού", υπό μορφή επιλογών και ερωτήσεων, απ' όπου ο     ;;;
;;; χρήστης εισάγει τα δεδομένα του προβλήματος της "κοπής του ξύλου", που θέλει να      ;;;
;;; λύσει κάθε φορά, καθώς και την υλοποίηση του αλγορίθμου "branch and bound" για το    ;;;
;;; συγκεκριμένο πρόβλημα.                                                               ;;;
;;;                                                                                      ;;;
;;;                                                                                      ;;;
;;; Ονομ/νυμο: Γιαννακόπουλος Παναγιώτης                                                 ;;;
;;; AM: 2615                                                                             ;;;
;;; e-mail: giannakp@ceid.upatras.gr                                                     ;;;
;;;**************************************************************************************;;;

;; καθολικές μεταβλητές

;; η επιφάνεια του διαθέσιμου φύλλου ξύλου
(defvar *M*)
;; ο αριθμός των κομματιών που θέλουμς να κόψουμε
(defvar *N*)
;; τα κομμάτια που θέλουμε να κόψουμε
(defvar *pieces*)
;; τα κομμάτια που θέλουμε να κόψουμε, ταξινομημένα κατά αύξουσα σειρά επιφάνειας
(defvar *sorted-pieces*)
;; ο αριθμός των λύσεων που ζήτησε ο χρήστης, να βρει ο αλγόριθμος
(defvar *max-solutions*)

;; το μέτωπο αναζήτησης
(defvar *frontier*)
;; η τρέχουσα κατάσταση
(defvar *current-state*)
;; το τρέχον περίσσευμα
(defvar *current-waste*)
;; η μεταβλητή αυτή χρησιμοποιείται για να αποθηκεύσουμε όλες τις καταστάσεις του
;; προβλήματος (χώρου αναζήτησης)
(defvar *states*)
;; η μεταβλητή αυτή περιέχει όλες τις καταστάσεις του προβλήματος χωρισμένες σε λίστες
;; ανά μήκος κατάστασης (λίστας)
(defvar *states-per-length*)
;; στη μεταβλητή *solutions* αποθηκεύονται όλες οι λύσεις του προβλήματος
(defvar *solutions*)

;;;**************************************************************************************;;;
;;; Η κύρια συνάρτηση του αρχείου. Μέσω της συνάρτησης αυτής και των συναρτήσεων που     ;;;
;;; αυτή περιλαμβάνει και καλεί, ο χρήστης εισάγει τα δεδομένα του προβλήματος που θέλει ;;;
;;; κάθε φορά να λύσει, δηλαδή την διαθέσιμη επιφάνεια του φύλλου ξύλου Μ καθώς και τα Ν ;;;
;;; κομμάτια που επιθυμεί να κόψει. Η εισαγωγή των δεδομένων αυτών γίνεται είτε μέσω του ;;;
;;; πληκτρολογίου είτε μέσω ενός αρχείου ανάλογα με την επιλογή του χρήστη. Επίσης ο     ;;;
;;; χρήστης ερωτάται εάν θέλει ο αλγόριθμος να βρεί περισσότερες από μια λύσεις, εάν     ;;;
;;; αυτό είναι εφικτό. Στη συνέχεια ελέγχεται εάν το πρόβλημα, με τα δεδομένα που έδωσε  ;;;
;;; ο χρήστης, χρειάζεται επίλυση και εάν χρειάζεται καλείται ο αλγόριθμος branch and    ;;;
;;; bound για να λύσει το πρόβλημα.                                                      ;;;
;;;**************************************************************************************;;;
(defun menu ()
	;; αρχικοποιήσεις των καθολικών μεταβλητών
  	(setf *M* nil)
	(setf *N* nil)
	(setf *pieces* nil)
	(setf *sorted-pieces* nil)
	(setf *max-solutions* nil)
	(setf *frontier* nil)
	(setf *current-state* nil)
	(setf *current-waste* nil)
	(setf *states* nil)
	(setf *states-per-length* nil)
	(setf *solutions* nil)

	(format t "~2%	'Wood Cutting problem' - Branch & Bound algorithm~3%")
	(format t "~&Please specify the way to get problem's data:~%")
	(format t "~&(1) via keyboard")
	(format t "~&(2) via a file")
	(format t "~&Enter your choice here > ")
	(let ((input nil))
		(loop
			(setf input (read))
			(if (numberp input)
				(cond
					((= input 1)
						(setf *M* (get-problem-data "Enter a positive value for the surface of the wood sheet (M) > "))
						(setf *N* (get-problem-data "Now enter how many pieces you would like to cut (N) > "))
						(get-pieces-surfaces) (return)
					)
					((= input 2) (get-problem-data-from-file) (return))
					(t (format t "~&Please enter only 1 or 2 > "))
				)
				(format t "~&You must enter a number! Please enter your choice again > ")
			)
		)
	)
	(get-max-solutions)

	(format t "~%Starting the algorithm with the following data")
	(format t "~&Surface of wood sheet to be cut: ~S" *M*)
	(format t "~&Pieces we would like to cut: ~S" *pieces*)
	(format t "~&Maximum solutions to find: ~S ~[(all solutions) ~;~]" *max-solutions* (if (= *max-solutions* 0) 0 1))
	(check-if-problem-needs-solving)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή παίρνει τα δεδομένα που δίνει ο χρήστης από το πληκτρολόγιο, για το ;;;
;;; Μ και το Ν, και θέτει τις αντίστοιχες καθολικές μεταβλητές *Μ* και *Ν*.              ;;;
;;;**************************************************************************************;;;
(defun get-problem-data (string-message)
	(format t "~&~A" string-message)
	(let ((input nil))
		(loop
			(setf input (read))
			(if (numberp input)
				(if (<= input 0)
					(format t "~&Value can't be negative or zero! Please enter a positive value > ")
					(return-from get-problem-data input)
				)
				(format t "~&You must enter a number! Please enter your choice again > ")
			)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή κάνει την ίδια δουλειά με την πάνω μόνο που τα δεδομένα τώρα        ;;;
;;; εισάγονται μέσω ενός αρχείου που καθορίζει ο χρήστης.                                ;;;
;;;**************************************************************************************;;;
(defun get-problem-data-from-file ()
	(let ((filename nil))
		(format t "~&Note 1: the file must have one line with the number M (surface of the wood sheet) and one line below it with a list of the desired pieces to cut.")
		(format t "~&Note 2: the path to the file must be in the following format: hard-drive-letter:/path-to-file/filename")
		(format t "~&Enter the file to read from > ")
		(loop
			(setf filename (read-line))
			(if (probe-file filename)
				(with-open-file (stream filename)
					(let ((M (read stream))
						  (pieces (read stream)))
						(setf *M* M)
						(setf *pieces* pieces)
					)
					(format t "~&Data read successfully!")
					(return)
				)
				(format t "~&File doesn't exist! Please enter an existing file > ")
			)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή παίρνει τα Ν κομμάτια που δίνει ο χρήστης από το πληκτρολόγιο και   ;;;
;; τα αποθηκεύει στην καθολική μεταβλητή *pieces*.                                       ;;;
;;;**************************************************************************************;;;
(defun get-pieces-surfaces ()
	(format t "~&Now enter the surfaces of each piece ~%")
	(loop
		(when (= (length *pieces*) *N*) (return))
		(format t " > ")
		(let ((input (read)))
			(if (numberp input)
				(if (<= input 0)
					(format t "~&Value can't be negative or zero! Please enter a positive value > ")
					(setf *pieces* (append *pieces* (list input)))
				)
				(format t "~&You must enter a number! Please enter your choice again > ")
			)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή παίρνει τον επιθυμητό αριθμό λύσεων που θέλει ο χρήστης να βρει ο   ;;;
;;; αλγόριθμος και την αποθηκεύει στην καθολική μεταβλητή *max-solutions*.               ;;;
;;;**************************************************************************************;;;
(defun get-max-solutions ()
	(format t "~&Enter how many solutions the algorithm must find (if possible). (enter 0 for all solutions) > ")
	(loop
		(setf *max-solutions* (read))
		(if (numberp *max-solutions*)
			(if (< *max-solutions* 0)
				(format t "~&Value can't be negative! Please enter a positive value or zero > ")
				(return)
			)
			(format t "~&You must enter a number! Please enter your choice again > ")
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή ελέγχει εάν το πρόβλημα με τα δεδομένα που έδωσε ο χρήστης για το Μ ;;;
;;; και το Ν, χρειάζεται όντως επίλυση. Δηλαδή ελέγχει εάν το άθροισμα των επιφανειών    ;;;
;;; των κομματιών είναι όντως μεγαλύτερο από την διαθέσιμη επιφάνεια Μ του φύλλου ξύλου  ;;;
;;; οπότε και επιλύουμε το πρόβλημα, διαφορετικά εάν είναι μικρότερο ή ίσο μπορούμε απλά ;;;
;;; να επιλέξουμε όλα τα κομμάτια και δεν χρειάζεται να επιλύσουμε το πρόβλημα.          ;;;
;;;**************************************************************************************;;;
(defun check-if-problem-needs-solving ()
	(if (<= (apply '+ *pieces*) *M*)
		(format t "~&Problem is trivial... just select all the pieces! :)")
		(branch-and-bound)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή αποτελεί το ξεκίνημα του αλγορίθμου branch and bound.				 ;;;
;;;**************************************************************************************;;;
(defun branch-and-bound ()
	;; ταξινομούμε κατά αύξουσα σειρά επιφάνειας τα κομμάτια που έδωσε ο χρήστης
	(setf *sorted-pieces* (sort *pieces* #'<))
	;; θέτουμε το μικρότερο από αυτά στο μέτωπο αναζήτησης
	(setf *frontier* (list (list (first *sorted-pieces*))))
	(loop
		;; εάν το μέτωπο αναζήτησης είναι κενό τερματίζουμε
		(when (null *frontier*) (return))
		;; αλλιώς θέτουμε σαν τρέχουσα κατάσταση την πρώτη κατάσταση του μετώπου αναζήτησης
		(setf *current-state* (pop *frontier*))
		;; υπολογίζουμε το τρέχον περίσσευμα
		(setf *current-waste* (- *M* (apply '+ *current-state*)))
		;(format t "~&frontier (2) = ~S" *frontier*)
		(add-state)
		(expand)
	)
	(format t "~2%Problem solved!~2%")
	(show-solutions-and-print-search-tree)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή προσθέτει την τρέχουσα κατάσταση στην καθολική μεταβλητή *states*   ;;;
;;; που περιέχει τις καταστάσεις που έχουμε επισκεφθεί μέχρι στιγμής, ταξινομώντας την   ;;;
;;; συγχρόνως μέσω ενός αλγορίθμου παρεμβολής. Τελικά αφού έχουμε επισκεφτεί όλες τις    ;;;
;;; καταστάσεις του χώρου αναζήτησης η καθολική μεταβλητή *states* περιέχει όλες αυτές   ;;;
;;; τις καταστάσεις ταξινομημένες κατά δύο τρόπους, κατά πρώτον ως προς το μήκος των     ;;;
;;; καταστάσεων (λιστών), κατά φθίνουσα σειρά (από το μεγαλύτερο μήκος στο μικρότερο)    ;;;
;;; και κατά δεύτερον ως το προς το περίσσευμα κατά αύξουσα σειρά (από το μικρότερο      ;;;
;;; περίσσευμα στο μεγαλύτερο). Από τα παραπάνω συμπεραίνουμε ότι η/οι λύση/εις θα       ;;;
;;; βρίσκεται/ονται στην αρχή της μεταβλητής (λίστας) *states*.                          ;;;
;;;**************************************************************************************;;;
(defun add-state ()
	(let* ((length-of-current-state (length *current-state*)) (first-of-states (first *states*)) (length-of-first-of-states (length first-of-states)))
		(if (null *states*)
			(push *current-state* *states*)
			(cond
				((> length-of-current-state length-of-first-of-states)
					(push *current-state* *states*)
				)
				((= length-of-current-state length-of-first-of-states)
					(dolist (elem *states*)
						(if (and (= length-of-current-state (length elem)) (< *current-waste* (- *M* (apply '+ elem))))
							(let ((index (- (position elem *states*) 1)))
								(if (minusp index)
									(progn (push *current-state* *states*) (return))
									(progn (push *current-state* (rest (nthcdr index *states*))) (return))
								)
							)
						)
					)
				)
				((< length-of-current-state length-of-first-of-states)
					(dolist (elem *states*)
						(if (and (= length-of-current-state (length elem)) (< *current-waste* (- *M* (apply '+ elem))))
							(progn (push *current-state* (rest (nthcdr (- (position elem *states*) 1) *states*))) (return))
						)
						(if (> length-of-current-state (length elem))
							(progn (push *current-state* (rest (nthcdr (- (position elem *states*) 1) *states*))) (return))
						)
					)
				)
			)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή είναι ουσιαστικά η καρδιά του αλγορίθμου branch and bound αφού      ;;;
;;; επιτελεί τις δύο ουσιώδεις λειτουργίες του. Καταρχήν είναι υπεύθυνη για την παραγωγή ;;;
;;; των καταστάσεων-παιδιών από την κατάσταση-πατέρα, το οποίο αποτελεί και το branch    ;;;
;;; (διακλάδωση) κομμάτι του αλγορίθμου. Και κατά δεύτερον μέσω της εφαρμογής του        ;;;
;;; περιορισμού, ότι το άθροισμα των επιφανειών των κομματιών δεν πρέπει να υπερβαίνει   ;;;
;;; την συνολική διαθέσιμη επιφάνεια Μ του φύλλου του ξύλου, πραγματοποιείται και το     ;;;
;;; bound (οριοθέτηση) κομμάτι του αλγορίθμου αφού κλαδεύονται όλες αυτές οι καταστάσεις ;;;
;;; που δεν ικανοποιούν τον παραπάνω περιορισμό.                                         ;;;
;;;**************************************************************************************;;;
(defun expand ()
	(let* ((last-number-of-state (first (last *current-state*))) (temp-list (member last-number-of-state *sorted-pieces*))
		   (counter1 (count last-number-of-state *current-state*)) (counter2 (count last-number-of-state temp-list))
		   (children-states nil) (index nil))
	;; διακλάδωση (branch)
		;; αφαίρεσε τα διπλότυπα, εάν υπάρχουν, από την temp-list
		(setf temp-list (remove-duplicates temp-list))
		(if (= counter1 counter2)
			(setf temp-list (rest temp-list))
		)
		;; παρήγαγε τις καταστάσεις-παιδιά
		(dolist (elem temp-list)
			(setf children-states (append children-states (list (append *current-state* (list elem)))))
		)

	;; bound
		(dolist (elem children-states)
			(setf index (position elem children-states))
			(if (> (apply '+ elem) *M*)
				;; αφαίρεσε (κλάδεψε) από τις καταστάσεις-παιδιά, όλες αυτές των οποίων
				;; το άθροισμα των στοιχείων τους υπερβαίνει την διαθέσιμη επιφάνεια του ξύλου (*Μ*).
				(setf children-states (append (reverse (set-difference children-states (nthcdr index children-states))) (nthcdr (+ index 1) children-states)))
			)
		)

		;; θέσε τις καταστάσεις-παιδιά που μένουν, μπροστά στο μέτωπο της αναζήτησης
		(setf *frontier* (append children-states *frontier*))
	)
)


;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή επιτελεί δύο λειτουργίες. Κατά πρώτον εμφανίζει την/τις λύση/εις    ;;;
;;; του προβλήματος, ανάλογα πάντα με το πόσες λύσεις υπάρχουν και με το τί έχει ζητήσει ;;;
;;; ο χρήστης. Επίσης (εάν ο χρήστης το επέλεξε) καλεί την συνάρτηση print-search-tree η ;;;
;;; οποία τυπώνει το δέντρο αναζήτησης στην οθόνη ή σε κάποιο αρχείο ανάλογα με την      ;;;
;;; επιλογή του χρήστη.                                                                  ;;;
;;;**************************************************************************************;;;
(defun show-solutions-and-print-search-tree ()
	;; Το ακόλουθο κομμάτι κώδικα επιτελεί την εξής λειτουργία. Χωρίζει την καθολική μεταβλητή
	;; *states* (η οποία περιέχει όλες τις καταστάσεις του προβήματος ταξινομημένες όπως
	;; περιγράφηκαν στη συνάρτηση add-state) σε τόσες λίστες όσα είναι και τα διαφορετικά
	;; μήκη καταστάσεων και τις αποθηκεύει στην μεταβλητή *states-per-length* κατά αύξουσα
	;; σειρά (το τελευταίο στοιχείο της μεταβλητής *states-per-length* περιέχει την λίστα
	;; με τις υπολίστες του μεγαλυτερου μήκους).
	(let ((state-length (length (first *states*))) (temp-list nil))
		(dolist (state *states*)
			(if (< (length state) state-length)
				(progn
					(setf state-length (- state-length 1))
					(push temp-list *states-per-length*)
					(setf temp-list nil)
				)
			)
			(if (equal state (first (last *states*)))
				(push (list state) *states-per-length*)
			)
			(push state temp-list)
		)
	)

	;; Σεο ακόλουθο κομμάτι κώδικα εξάγουμε τις λύσεις του προβλήματος. Αυτές θα περιέχονται
	;; στο τελευταίο στοιχείο της μεταβλητής *states-per-length*, το οποίο και αποθηκεύουμε
	;; στην καθολική μεταβλητή *solutions*. Επίσης παίρνουμε το reverse έτσι ώστε η καλύτερη
	;; λύση να ειναι το πρώτο στοιχείο της *solutions*, το οποίο είναι μια λίστα με τα
	;; περισσότερα κομμάτια που μπορούμε να κόψουμε και με το μικρότερο περίσσευμα. Εάν
	;; υπάρχουν παραπάνω από μια λύσεις αυτές θα πρέπει να έχουν το ίδιο περίσσευμα με την
	;; πρώτη οπότε και μετράμέ πόσες υπάρχουν. Τέλος εμφανίζουμε την/τις λύση/εις.
	(setf *solutions* (reverse (first (last *states-per-length*))))
	(let* ((all-solutions 1) (first-best-solution (first *solutions*)) (min-waste (- *M* (apply '+ first-best-solution))))
		(dolist (solution *solutions*)
			(if (and (not (equal first-best-solution solution)) (= min-waste (- *M* (apply '+ solution))))
				(incf all-solutions)
			)
		)
		(cond
			((= all-solutions 1)
				(format t "~&There is only 1 (optimal) solution to the problem!")
				(format t "~&The solution is to cut the pieces ~S with a waste of ~S" first-best-solution min-waste)
			)
			((and (> *max-solutions* 0) (< *max-solutions* all-solutions))
				(setf all-solutions *max-solutions*)
				(format t "~&Below are ~D of the solutions to the problem" *max-solutions*)
			)
			((or (>= *max-solutions* all-solutions) (= *max-solutions* 0)) (format t "~&Below are all the solutions to the problem"))
		)
		(if (> all-solutions 1)
			(do ((index 0 (+ index 1)))
				((= index all-solutions))
				(let ((solution (nth index *solutions*)))
					(format t "~&Solution #~D is to cut the pieces ~S with a waste of ~S" (+ index 1) solution (- *M* (apply '+ solution)))
				)
			)
		)
	)

	;; Τέλος ρωτάμε τον χρήστη εάν επιθυμεί να εκτυπωθεί το δέντρο αναζήτησης. Εάν επιθυμεί
	;; καλούμε την συνάρτηση print-search-tree με την μεταβλητή *states-per-length* ως πρώτο
	;; όρισμα και με δεύτερο όρισμα, nil εάν θέλει να εκτυπωθεί το δέντρο στην οθόνη ή με την
	;; μεταβλητή filename εάν θέλει να εκτυπωθεί στο αντίστοιχο αρχείο.
	(format t "~2%Would you like the search tree to be printed? (select yes or no in the opening window)~2%")
	(let ((input nil) (filename nil))
		(setf input (yes-or-no-p))
		(if (equal input t)
			(progn
				(format t "~&Where would you like the tree to be printed? ")
				(format t "~&(1) to screen")
				(format t "~&(2) to a file")
				(format t "~&Enter your choice here > ")
				(loop
					(setf input (read))
					(if (numberp input)
						(cond
							((= input 1) (format t "~%Note: if the tree is too wide, it won't appear good due to wrapping.")
										 (format t "~&In such a case better save it to a file and view it with a fixed width font and word wrap disabled.")
										 (format t "~&The search tree is shown below~2%") (print-search-tree *states-per-length* nil) (return))
							((= input 2) (format t "~&Note: the path to the file must be in the following format: hard-drive-letter:/path-to-file/filename")
										 (format t "~&Enter the file to save to > ") (setf filename (read-line)) (format t "~&Writing search tree to file...~%")
										 (print-search-tree *states-per-length* filename) (return))
							(t (format t "~&Please enter only 1 or 2. Try again > "))
						)
						(format t "~&You must enter a number! Please enter your choice again > ")
					)
				)
				'Done!
			)
			'Done!
		)
	)
)
