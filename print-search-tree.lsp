;;;**************************************************************************************;;;
;;;                                       Περιγραφή                                      ;;;
;;;                                                                                      ;;;
;;; Τυπώνει το δέντρο αναζήτησης (στην οθόνη ή σε δοθέν αρχείο). Είναι υλοποιημένο για   ;;;
;;; το πρόβλημα της κοπής του ξύλου, αν και έχει υλοποιηθεί με γενικό τρόπο και πιθανόν  ;;;
;;; να τυπώνει και δέντρα αναζήτησης για άλλα προβλήματα αλλά ωστόσο δε το έχω δοκιμάσει ;;;
;;; . Εν συντομία η λειτουργία του συγκεκριμένου κώδικα είναι η εξής. Δέχεται ως είσοδο  ;;;
;;; μια λίστα με όλες τις καταστάσεις (όλους του συνδυασμούς/κόμβους δέντρου) από το     ;;;
;;; πρόβλημα της κοπής του ξύλου. Στη συνέχεια ταξινομεί (μέσω της συνάρτησης            ;;;
;;; sort-tree-nodes) αυτές τις λύσεις/κόμβους, μέ έναν αλγόριθμο παρεμβολής, έτσι ώστε   ;;;
;;; να μπορούν να εκτυπωθούν σαν να διατρέχαμε το δέντρο με ένα αλγόριθμο πρώτα κατά     ;;;
;;; πλάτος, αφού η σάρωση/εκτύπωση γίνεται ευκολότερα από αριστερά προς τα δεξιά και από ;;;
;;; πάνω προς τα κάτω παρά από πάνω προς τα κάτω και από αριστερά προς τα δεξιά (όπως    ;;;
;;; δουλεύει ο αλγόριθμος πρώτα κατά βάθος). Το τελικό αποτέλεσμα είναι μια λίστα με     ;;;
;;; τόσες υπολίστες όσα είναι τα επίπεδα του δέντρου και όπου η κάθε μία υπολίστα (που   ;;;
;;; αναπαριστά ένα επίπεδο στο δέντρο) περιέχει τόσες άλλες υπολίστες όσες είναι οι      ;;;
;;; κόμβοι του συγκεκριμένου επιπέδου, στη μορφή (πατέρας παιδί ... παιδί). Στη συνέχεια ;;;
;;; καλείται η συνάρτηση calculate-nodes-and-branches-printing-marks για κάθε πίπεδο,    ;;;
;;; ξεκινώντας από το τελευταίο που ισούται με το βάθος του δέντρου και πηγαίνοντας προς ;;;
;;; το πρώτο επίπεδο που βρίσκεται η ρίζα του δέντρου (επίπεδο 0). Η συνάρτηση αυτή      ;;;
;;; υπολογίζει και αποθηκεύει αρχικά τα σημάδια εκτύπωσης (δηλάδη τα σημεία (ξεκινώντας  ;;;
;;; από το 0) στα οποία θα εκτυπώσουμε ένα χαρακτήρα) για τους κόμβους και τα κλαδιά     ;;;
;;; του δέντρου από τον αριστερότερο κόμβο και προς τα δεξιά. Μόλις τελειώσει ο βρόχος,  ;;;
;;; έχουμε υπολογίσει και αποθηκεύσει στις αντίστοιχες λίστες τα σημάδια που θα          ;;;
;;; χρειαστούμε για την εκτύπωση του δέντρου, τα οποία όμως περιέχουν λανθασμένες τιμές  ;;;
;;; για κόμβους που δεν είναι ακολουθιακοί μεταξύ τους. Γι' αυτό στη συνέχεια, εφ' όσον  ;;;
;;; γνωρίζουμε τον πατέρα κάθε κόμβου διατρέχουμε πάλι μέσω ενός βρόχου όλα τα επίπεδα   ;;;
;;; από το τελευταίο μέχρι το πρώτο (0) και διορθώνουμε στην πορεία όσες τιμές είναι     ;;;
;;; λάθος για τα σημάδια εκτύπωσης των κόμβων και των κλαδιών. Αυτή η διαδικάσία         ;;;
;;; επαναλαμβάνεται όσες φορές χρειαστεί μέχρι να διορθωθούν όλες οι λάθος τιμές και     ;;;
;;; σταματά μόλις δεν υπάρξει καμία διόρθωση. Τέλος χρησιμοποιώντας τα σημάδια αυτά και  ;;;
;;; τις συναρτήσεις print-node-numbers, print-vertical-branches και                      ;;;
;;; print-horizontal-branches εκτυπώνουμε, επίπεδο-επίπεδο, τους κόμβους (αριθμούς) και  ;;;
;;; τα κλαδιά (οριζόντιες και κάθετες γραμμές) στις αντίστοιχες θέσεις οπότε τελικά      ;;;
;;; έχουμε εκτυπώσει όλο το δέντρο αναζήτησης (με έναν όμορφο τρόπο :-).                 ;;;
;;;                                                                                      ;;;
;;;	Να σημειώσω τέλος ότι μετά από μερικές δοκιμές που έκανα για αρκετά μεγάλα δέντρα,   ;;;
;;; ενώ τα σημάδια υπολογίζονται σωστά, όταν γίνεται η εκτύπωσή τους, αυτή δεν           ;;;
;;; τερματίζει ποτέ. Δεν ξέρω τι ακριβώς φταίει αλλά πιστεύω πως δεν είναι κάποιο bug    ;;;
;;; γιατί αν ήταν το παραπάνω θα γινόταν και με μικρότερου πλάτους δέντρα. Θεωρώ πως     ;;;
;;; πρέπει να οφείλεται στο περιορισμένο (από το σύστημα αρχείων) εύρος που μπορεί να    ;;;
;;; έχει μια γραμμή σε ένα αρχείου κειμένου.                                             ;;;
;;;                                                                                      ;;;
;;;                                                                                      ;;;
;;; Ονομ/νυμο: Γιαννακόπουλος Παναγιώτης                                                 ;;;
;;; AM: 2615                                                                             ;;;
;;; e-mail: giannakp@ceid.upatras.gr                                                     ;;;
;;;**************************************************************************************;;;

;; καθολικές μεταβλητές

;; στη μεταβλητή αυτή περιέχονται όλοι οι κόμβοι του δέντρου για όλα τα επίπεδα
(defvar *all-levels-parents-children*)
;; το βάθος του δέντρου (= αριθμός επιπέδων δέντρου - 1)
(defvar *tree-depth*)
;; μεταβλητή στην οποία αποθηκεύεται η τιμή της απόστασης που αφήσαμε από
;; την αρχή μέχρι τον πρώτο κόμβο του επιπέδου για το προηγούμενο επίπεδο
(defvar *previous-indent*)
;; ο μετρητής που χρησιμοποιούμε για να σαρώσουμε (εικονικά) του κόμβους και
;; να θέσουμε τα σημάδια εκτύπωσης
(defvar *print-counter*)
;; παρακάτω ορίζονται οι λίστες στις οποίες αποθηκεύουμε τα σημάδια εκτύπωσης
;; για το προηγούμενο επίπεδο
(defvar *previous-level-nodes-horizontal-branch-marks*)
(defvar *previous-level-nodes-upper-branch-marks*)
(defvar *previous-level-nodes-number-marks*)
(defvar *previous-level-nodes-lower-branch-marks*)
;; παρακάτω ορίζονται οι λίστες στις οποίες αποθηκεύουμε τα σημάδια εκτύπωσης
;; για όλα τα επίπεδα
(defvar *nodes-horizontal-branch-marks*)
(defvar *nodes-upper-branch-marks*)
(defvar *nodes-number-marks*)
(defvar *nodes-lower-branch-marks*)

;;;**************************************************************************************;;;
;;; Η κύρια συνάρτηση του αρχείου. Περιέχει 4 βασικά σημεία/κλήσεις συναρτήσεων          ;;;
;;; (σημειώνονται και παρακάτω). (1) Καλείται η συνάρτηση sort-tree-nodes και γίνεται η  ;;;
;;; ταξινόμηση των κόμβων (βλέπε περιγραφή πάνω) ώστε να είναι κατάλληλοι για σάρωση και ;;;
;;; υπολογιμό των σημαδιών εκτύπωσης. (2) Καλείται η συνάρτηση                           ;;;
;;; calculate-nodes-and-branches-printing-marks η οποία υπολογίζει και αποθηκεύει σε μια ;;;
;;; πρώτη φάση τα περισσότερα σημάδια για τους κόμβους και τα κλαδιά του δέντρου. (3)    ;;;
;;; Καλείται η συνάρτηση fix-misplaced-nodes-and-branches για κάθε επίπεδο απο το        ;;;
;;; τελευταίο προς το πρώτο και αυτό γίνεται όσες φορές χρειαζτεί μέχρι να μην υπάρξει   ;;;
;;; καμία διόρθωση. (4) Καλείται η συνάρτηση print-the-tree με το κατάλληλο όρισμα       ;;;
;;; (ανάλογα με το αν θέλουμε να τυπώσουμε το δέντρο στην οθόνη ή σε αρχείο) η οποία     ;;;
;;; καλεί τις συναρτήσεις που τυπώνουν τα σημάδια για τους κόμβους και τα φύλλα.         ;;;
;;;**************************************************************************************;;;
(defun print-search-tree (nodes filename)
	;; αρχικοποιήσεις των καθολικών μεταβλητών
	(setf *all-levels-parents-children* nil)
	(setf *tree-depth* nil)
	(setf *previous-indent* nil)
	(setf *print-counter* nil)
	(setf *previous-level-nodes-lower-branch-marks* nil)
	(setf *previous-level-nodes-number-marks* nil)
	(setf *previous-level-nodes-upper-branch-marks* nil)
	(setf *previous-level-nodes-horizontal-branch-marks* nil)
	(setf *nodes-lower-branch-marks* nil)
	(setf *nodes-number-marks* nil)
	(setf *nodes-upper-branch-marks* nil)
	(setf *nodes-horizontal-branch-marks* nil)

	;; (1)
	(dolist (elem nodes)
		(sort-tree-nodes elem)
	)

	;; (2)
	(setf *tree-depth* (- (length *all-levels-parents-children*) 1))
	(let ((level *tree-depth*))
		(dolist (elem *all-levels-parents-children*)
			(calculate-nodes-and-branches-printing-marks elem level)
			(decf level)
		)
	)

	; (format t "~%~%")
	; (debugging "*nodes-lower-branch-marks*" *nodes-lower-branch-marks*)
	; (debugging "*nodes-number-marks*" *nodes-number-marks*)
	; (debugging "*nodes-upper-branch-marks*" *nodes-upper-branch-marks*)
	; (debugging "*nodes-horizontal-branch-marks*" *nodes-horizontal-branch-marks*)

	;; (3) Κάνε όσα περάσματα είναι απαραίτητα έτσι ώστε να διορθώσεις όλους τους κόμβους και τα κλαδιά
    ;; που είναι τοποθετημένα λάθος (τα σημάδια εκτύπωσης έχουν λάθος τιμές).
	(let ((num-of-fixes 0))
		(loop
			(dotimes (n *tree-depth*)
				;; Ξεκινάμε από ένα επίπεδο πάνω από το βάθος του δέντρου αφού οι διορθώσεις γίνονται
				;; στο κατώτερο επίπεδο από το τρέχον και το τελευταίο επίπεδο δεν έχει κατώτερο από αυτό.
				(setf num-of-fixes (+ num-of-fixes (fix-misplaced-nodes-and-branches (- *tree-depth* n 1) num-of-fixes)))
			)
			;(format t "~&Number of fixes = ~S~%" num-of-fixes)
			(when (= num-of-fixes 0) (return))
			(setf num-of-fixes 0)
		)
	)

	(convert-list-for-printing *nodes-upper-branch-marks*)
	(convert-list-for-printing *nodes-number-marks*)
	(convert-list-for-printing *nodes-lower-branch-marks*)

	; (format t "~%~%")
	; (debugging "*nodes-lower-branch-marks*" *nodes-lower-branch-marks*)
	; (debugging "*nodes-number-marks*" *nodes-number-marks*)
	; (debugging "*nodes-upper-branch-marks*" *nodes-upper-branch-marks*)
	; (debugging "*nodes-horizontal-branch-marks*" *nodes-horizontal-branch-marks*)

	;; (4) Τυπώνουμε το δέντρο στην οθόνη ή στο αρχείο που έχει καθορίσει ο χρήστης
	(if (null filename)
		(print-the-tree t)
		(with-open-file (stream filename :direction :output :if-does-not-exist :create :if-exists :supersede)
			(print-the-tree stream)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή, όπως έχουμε προαναφέρει, δέχεται τις λύσεις (κόμβους δέντρου) από  ;;;
;;; την έξοδο του αλγορίθμου branch and bound οι οποίες περιέχονται σε μια λίστα για     ;;;
;;; κάθε επίπεδο και τις μετατρέπει κατάλληλα, αφού τις ταξινομήσει με έναν αλγόριθμο    ;;;
;;; παρεμβολής, έτσι ώστε να μπορούμε να τις επεξεργαστούμε για να υπολογίσουμε τα       ;;;
;;; σημάδια εκτύπωσης. Στο τέλος αποθηκεύονται στην καθολική μεταβλητή                   ;;;
;;; *all-levels-parents-children* τόσες λίστες όσα είναι και τα επίπεδα του δέντρου, μια ;;;
;;; για κάθε επίπεδο, οι οποίες περιέχουν τόσες άλλες υπολίστες όσοι είναι και οι κόμβοι ;;;
;;; του δέντρου στο εκάστοτε επίεπδο και οι οποίες είναι της μορφής                      ;;;
;;; (πατέρας παιδί ... παιδί), δηλαδή το πρώτο στοιχείο της λίστας είναι ο πατέρας του   ;;;
;;; κόμβου, που βρίσκεται στο ανώτερο επίπεδο, ακολουθούμενος από όλα τα παιδιά του.     ;;;
;;;**************************************************************************************;;;
(defun sort-tree-nodes (nodes-to-be-sorted)

	(let ((buffer-list nil))
		(loop
			(when (null nodes-to-be-sorted) (return))
			(let ((sorted-list-temp (list (pop nodes-to-be-sorted))) (unsorted-list-temp nodes-to-be-sorted))
				(dolist (element unsorted-list-temp)
					(let ((temp (mapcar '= (butlast element) (butlast (first sorted-list-temp)))) (flag t) (index (position element nodes-to-be-sorted)))
						(dolist (elem temp)
							(setf flag (and flag elem))
						)
						(if (equal flag t)
							(progn
								;;remove element from nodes-to-be-sorted
								(setf nodes-to-be-sorted (append (reverse (set-difference nodes-to-be-sorted (nthcdr index nodes-to-be-sorted))) (nthcdr (+ index 1) nodes-to-be-sorted)))
								(dolist (elem sorted-list-temp)
									(if (<= (first (last element)) (first (last elem)))
										(progn
											(if (equal elem (first sorted-list-temp))
												(progn (push element sorted-list-temp) (return))
												(progn (push element (rest (nthcdr (- (position elem sorted-list-temp) 1) sorted-list-temp))) (return))
											)
										)
									)
									(if (equal elem (first (last sorted-list-temp)))
										(progn (push element (rest (nthcdr (- (length sorted-list-temp) 1) sorted-list-temp))) (return))
									)
								)
							)
						)
					)
				)

				(if (null buffer-list)
					(push sorted-list-temp buffer-list)
					(block insertion-sort
						(let* ((index 1) (number1 nil) (number2 nil) (first-sorted-elem (first sorted-list-temp)))
							(loop
								(when (= index (length first-sorted-elem)) (return-from insertion-sort))
								(setf number1 (nth index first-sorted-elem))
								(setf number2 (nth index (first (first buffer-list))))
								(if (< number1 number2)
									(progn (push sorted-list-temp buffer-list) (return-from insertion-sort))
								)
								(if (= number1 number2)
									(incf index)
								)
								(if (> number1 number2)
									(let ((elem-index 0) (elem nil) (next-elem nil) (list1 nil) (list2 nil) (list3 nil))
										(loop
											(when (= elem-index (length buffer-list)) (return))
											(setf elem (nth elem-index buffer-list))
											(setf next-elem (nth (+ (position elem buffer-list) 1) buffer-list))
											(setf list1 (nthcdr (- (length first-sorted-elem) index) (reverse first-sorted-elem)))
											(setf list2 (nthcdr (- (length (first elem)) index) (reverse (first elem))))
											(if (not (null next-elem))
												(setf list3 (nthcdr (- (length (first next-elem)) index) (reverse (first next-elem))))
											)

											(if (equal list1 list2)
												(progn
													(setf number1 (nth index first-sorted-elem))
													(setf number2 (nth index (first elem)))

													(if (< number1 number2)
														(progn
															(push sorted-list-temp (rest (nthcdr (- (position elem buffer-list) 1) buffer-list)))
															(return-from insertion-sort)
														)
													)
													(if (= number1 number2)
														(progn (incf index) (decf elem-index))
													)
													(if (equal elem (first (last buffer-list)))
														(progn
															(push sorted-list-temp (rest (nthcdr (- (length buffer-list) 1) buffer-list)))
															(return-from insertion-sort)
														)
													)
												)
											)
											(if (not (equal list1 list3))
												(progn
													(push sorted-list-temp (rest (nthcdr (position elem buffer-list) buffer-list)))
													(return-from insertion-sort)
												)
											)
											(incf elem-index)
										)
									)
								)
							)
						)
					)
				)
			)
		)
		(let ((sorted-nodes-list-temp nil) (temp nil))
			(dolist (elem buffer-list)
				(dolist (elem2 elem)
					(if (equal elem2 (first elem))
						(progn (push (second (reverse elem2)) temp) (setf temp (append temp (last elem2))))
						(setf temp (append temp (last elem2)))
					)
				)
				(setf sorted-nodes-list-temp (append sorted-nodes-list-temp (list temp)))
				(setf temp nil)
			)
			(push sorted-nodes-list-temp *all-levels-parents-children*)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή δέχεται το όρισμα-λίστα current-level-parents-children που είναι    ;;;
;;; μια λίστα της καθολικής μεταβλητής *all-levels-parents-children* και η οποία         ;;;
;;; αναπαριστά όλους του κόμβους (στη μορφή που έχουμε περιγράψει και πιο πάνω) του      ;;;
;;; τρέχοντος επιπέδου καθώς και το όρισμα level που περιέχει την τιμή του τρέχοντος     ;;;
;;; επιπέδου. Υπολογίζει αρχικά τις τιμές των σημαδιών εκτύπωσης για τους κόμβους και    ;;;
;;; τα κλαδιά του δέντρου και αποθηκεύει κάθε φορά τις υπολογισθείσες τιμές του          ;;;
;;; προηγούμενου (κατώτερου επιπέδου). Στο τέλος έχουν υπολογιστεί οι τιμές των σημαδιών ;;;
;;; εκτύπωσης και έχουν αποθηκευθεί στις αντίστοιχες καθολικές μεταβλητές. Στην          ;;;
;;; *nodes-horizontal-branch-marks* αποθηκεύονται τα σημάδια για την αρχή και το μήκος   ;;;
;;; των οριζόντιων κλαδιών του δέντρου (τυπώνονται με το σύμβολο '-'), στην              ;;;
;;; *nodes-upper-branch-marks* αποθηκεύονται τα σημάδια των κάθετων πάνω κλαδιών         ;;;
;;; (τυπώνονται με το σύμβολο '|'), στην *nodes-number-marks* αποθηκεύονται τα           ;;;
;;; σημάδια των κόμβων-αριθμών καθώς και οι ίδιοι αριθμοί και τέλος στην                 ;;;
;;; *nodes-lower-branch-marks* αποθηκεύονται τα σημάδια των κάθετων κάτω κλαδιών         ;;;
;;; (τυπώνονται με το σύμβολο '|') τα οποία ενώνουν τον πατέρα κόμβο με τους κόμβους     ;;;
;;; παιδιά του.                                                                          ;;;
;;;**************************************************************************************;;;
(defun calculate-nodes-and-branches-printing-marks (current-level-parents-children level)

	(if (< level *tree-depth*)
		(push-nils-to-marks)
	)
	(setf *print-counter* 0)

	(let* ((current-indent nil) (current-level-nodes-number-marks nil) (current-level-nodes-upper-branch-marks nil) (current-level-nodes-horizontal-branch-marks nil)
	       (current-level-nodes-branch-marks nil) (temp-list1 nil) (temp-list2 nil) (temp-list3 nil) (temp-list4 nil) (current-node-parent nil)
		   (current-node-children nil) (previous-level-parents-children nil) (copy-of-previous-level-parents-children nil) (current-distance-between-nodes nil)
		   (copy-of-previous-horizontal-branch-marks *previous-level-nodes-horizontal-branch-marks*) (correctional-distance nil))

		(if (< level *tree-depth*)
			(setf previous-level-parents-children (nth (- *tree-depth* level 1) *all-levels-parents-children*))
		)
		(setf copy-of-previous-level-parents-children previous-level-parents-children)

		(setf current-indent (calculate-indent *previous-level-nodes-horizontal-branch-marks*))

		(dolist (elem current-level-parents-children)
			(setf current-node-parent (first elem))
			(setf current-node-children (rest elem))

			(dolist (number current-node-children)
				;; όρισε σαν προεπιλεγμένη απόσταση μεταξύ των κόμβων ένα κένο
				(setf current-distance-between-nodes 1)

				;; εδώ υπολογίζουμε την απόσταση μεταξύ δύο συνεχόμενων κόμβων που είναι και οι δύο πατέρες
				(let ((next-number (nth (+ (position number current-node-children) 1) current-node-children)))
					(if (and (not (null next-number)) (> (length copy-of-previous-level-parents-children) 1))
						(progn
							(if (and (= number (first (first copy-of-previous-level-parents-children)))
									 (= next-number (first (second copy-of-previous-level-parents-children))))
								(let* ((first-horizontal-branch (first copy-of-previous-horizontal-branch-marks))
									   (second-horizontal-branch (second copy-of-previous-horizontal-branch-marks))
									   (half-of-first-horizontal-branch (/ (second first-horizontal-branch) 2))
									   (half-of-second-horizontal-branch (/ (second second-horizontal-branch) 2))
									   (num-of-spaces1 0)
									   (num-of-spaces2 0)
									   (half-length-of-current-number 0)
									   (half-length-of-next-number 0)
									   (number-length 0))
									(pop copy-of-previous-level-parents-children)
									(pop copy-of-previous-horizontal-branch-marks)

									(if (oddp (second first-horizontal-branch))
										(setf num-of-spaces1 (floor half-of-first-horizontal-branch))
										(setf num-of-spaces1 half-of-first-horizontal-branch)
									)
									(if (oddp (second second-horizontal-branch))
										(setf num-of-spaces2 (floor half-of-second-horizontal-branch))
										(setf num-of-spaces2 (- half-of-second-horizontal-branch 1))
									)
									(setf number-length (get-number-length number))
									(if (oddp number-length)
										(setf half-length-of-current-number (floor (/ number-length 2)))
										(setf half-length-of-current-number (/ number-length 2))
									)
									(setf number-length (get-number-length next-number))
									(if (oddp number-length)
										(setf half-length-of-next-number (floor (/ number-length 2)))
										(setf half-length-of-next-number (- (/ number-length 2) 1))
									)
									(setf num-of-spaces1 (- num-of-spaces1 half-length-of-current-number))
									(setf num-of-spaces2 (- num-of-spaces2 half-length-of-next-number))
									(setf current-distance-between-nodes (+ num-of-spaces1 num-of-spaces2 1))
								)
							)
							(if (and (= number (first (first copy-of-previous-level-parents-children)))
									 (not (= next-number (first (second copy-of-previous-level-parents-children)))))
								(progn
									(pop copy-of-previous-level-parents-children)
									(pop copy-of-previous-horizontal-branch-marks)
								)
							)
						)
					)
				)

				;; εδώ κάνουμε μια προδιόρθωση εάν ο πατέρας βρίσκεται αριστερότερα της μέσης του μήκους του οριζόντιου κλαδιού του,
				;; έτσι ώστε να μην υπολογίσουμε λάθος τιμές και για τους επόμενους από αυτόν κόμβους
				(let ((number-length nil) (half-of-number-length nil))
					(setf number-length (get-number-length number))
					(setf half-of-number-length (floor (/ number-length 2)))
					(if (and (not (null previous-level-parents-children)) (= number (first (first previous-level-parents-children))))
						(let ((temp-sum nil) (current-upper-branch-mark nil))
							(setf temp-sum (+ *print-counter* number-length))
							(setf current-upper-branch-mark (- temp-sum half-of-number-length 1))
							(if (< level *tree-depth*)
								(let* ((lower-level-horizontal-branch-marks *previous-level-nodes-horizontal-branch-marks*) (first-node-horizontal-branch (first lower-level-horizontal-branch-marks))
									   (node-start-point (first first-node-horizontal-branch)) (horizontal-branch-length (second first-node-horizontal-branch))
									   (middle-point nil) (distance-from-start-to-middle-point nil) (difference 0))
									(setf middle-point (get-middle-point-of-node-horizontal-branch horizontal-branch-length))
									(setf distance-from-start-to-middle-point (+ node-start-point middle-point))
									(setf difference (- (+ current-upper-branch-mark 1) distance-from-start-to-middle-point))
									(if (< difference 0)
										(progn
											(setf difference (abs difference))
											(update-print-counter difference)
											(setf correctional-distance difference)
										)
									)
								)
							)
							(setf temp-sum (+ *print-counter* number-length))
							;; πρόσθεσε το σημάδι για το κάθετο πάνω κλαδί
							(setf temp-list1 (append temp-list1 (list (- temp-sum half-of-number-length 1))))
							(pop previous-level-parents-children)
							;; εδώ προσθέτουμε τα υπολογισμένα σημάδια του προηγούμενου επιπέδου στις αντίστοιχες μεταβλητές
							(append-marks)
						)
					)
					;; πόσθεσε το σημάδι για τον κόμβο-αριθμό καθώς και τον ίδιο τον αριθμό (μαζί ως μια λίστα)
					(setf temp-list2 (append temp-list2 (list (list *print-counter* number))))
					(update-print-counter number-length)
					;; πρόσθεσε το σημάδι για το κάθετο κάτω κλαδί
					(setf temp-list3 (append temp-list3 (list (- *print-counter* half-of-number-length 1))))
					(if (not (equal number (first (last current-node-children))))
						(update-print-counter current-distance-between-nodes)
					)
				)
			)

			;; υπολόγισε το σημάδι του οριζόντιου κλαδιού καθώς και το μήκος του
			(let ((horizontal-branch-length nil) (temp nil))
				(if (null current-level-nodes-horizontal-branch-marks)
					(setf horizontal-branch-length (- *print-counter* current-indent))
					(progn
						(setf temp (- *print-counter* (+ (apply '+ (butlast (first (last current-level-nodes-horizontal-branch-marks)))))))
						(setf horizontal-branch-length (- temp 1))
						(if (not (null correctional-distance))
							(setf horizontal-branch-length (- horizontal-branch-length correctional-distance))
						)
					)
				)
				;; πρόσθεσε το σημάδι για το οριζόντιο κλαδί μαζί με το μήκος του και τον πατέρα του (όλα μαζί ως μια λίστα)
				(setf temp-list4 (append temp-list4 (list (list (- *print-counter* horizontal-branch-length) horizontal-branch-length current-node-parent))))
			)

			;; φτάσαμε στο τέλος του τρέχοντοσ κόμβου, άφησε ένα κενό
			(incf *print-counter*)

			(setf correctional-distance nil)
			(setf current-level-nodes-branch-marks (append current-level-nodes-branch-marks (list temp-list1)))
			(setf current-level-nodes-number-marks (append current-level-nodes-number-marks (list temp-list2)))
			(setf current-level-nodes-upper-branch-marks (append current-level-nodes-upper-branch-marks (list temp-list3)))
			(setf current-level-nodes-horizontal-branch-marks (append current-level-nodes-horizontal-branch-marks temp-list4))
			(setf temp-list1 nil)
			(setf temp-list2 nil)
			(setf temp-list3 nil)
			(setf temp-list4 nil)
		)

		;; αποθήκευεσε τα σημάδια του προηγούμενου (κατώτερου) επιπέδου στις αντίστοιχες καθολικές μεταβλητές
		(setf *previous-level-nodes-lower-branch-marks* current-level-nodes-branch-marks)
		(setf *previous-level-nodes-number-marks* current-level-nodes-number-marks)
		(setf *previous-level-nodes-upper-branch-marks* current-level-nodes-upper-branch-marks)
		(setf *previous-level-nodes-horizontal-branch-marks* current-level-nodes-horizontal-branch-marks)
		(setf *previous-indent* current-indent)

		(if (= level 0)
			(progn
				(push-nils-to-marks)
				;; εφόσον είμαστε στο ανώτερο επίπεδο και δεν πάει παραπάνω προσθέτουμε τα υπολογισμένα σημάδια αυτού
				;; του επιπέδου στις αντίστοιχες μεταβλητές (ουσιαστικά είναι τα σημάδια για τη ρίζα)
				(append-marks)
			)
		)
	)
)


;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή είναι υπεύθυνη για την διόρθωση των λανθασμένων τιμών των σημαδιών  ;;;
;;; εκτύπωσης των κόμβων και των κλαδιών. Δέχεται το όρισμα level που αναπαριστά το      ;;;
;;; τρέχον επίπεδο και το όρισμα num-of-fixes που αναπαριστά το πλήθος των διορθώσεων    ;;;
;;; που γίνονται το οποίο και επιστρέφει. Η λειτουργία της εν συντομία έχει ως εξής.     ;;;
;;; Ξεκινώντας από ένα επίπεδο πάνω από το κατώτατο επίπεδο του δέντρου ελέγχουμε εάν οι ;;;
;;; κόμβοι-πατέρες βρίσκονται στη σωστή θέση (στη μέση του μήκους του οριζόντιου κλαδιού ;;;
;;; τους) και εάν αυτό δεν συμβαίνει οι κόμβοι-παιδιά μετακινούνται όσες θέσεις          ;;;
;;; χρειάζεται για να βρίσκεται ο κόμβος-πατέρας τους ακριβώς στη μέση του οριζόντιου    ;;;
;;; κλαδιού. Για κάθε διόρθωση αυξάνουμε την μεταβλητή-coυnter num-of-fixes.             ;;;
;;;**************************************************************************************;;;
(defun fix-misplaced-nodes-and-branches (level num-of-fixes)

	(let ((lower-level-horizontal-branch-marks (nth (+ level 1) *nodes-horizontal-branch-marks*))
		  (current-level-nodes-branch-marks-temp (nth level *nodes-lower-branch-marks*)) (current-level-nodes-branch-marks nil)
		  (node-start-point nil) (horizontal-branch-length nil) (current-upper-branch-mark nil) (middle-point nil)
		  (distance-from-start-to-middle-point nil) (difference 0) (position-of-node nil) (node-to-fix nil) (node-length nil))

		(dolist (elem current-level-nodes-branch-marks-temp)
			(if (not (equal elem nil))
				(setf current-level-nodes-branch-marks (append current-level-nodes-branch-marks elem))
			)
		)

		(dolist (node lower-level-horizontal-branch-marks)
			(setf position-of-node (position node lower-level-horizontal-branch-marks))
			(setf node-start-point (first node))
			(setf horizontal-branch-length (second node))
			(setf current-upper-branch-mark (pop current-level-nodes-branch-marks))
			(setf middle-point (get-middle-point-of-node-horizontal-branch horizontal-branch-length))
			(setf distance-from-start-to-middle-point (+ node-start-point middle-point))
			(setf difference (- (+ current-upper-branch-mark 1) distance-from-start-to-middle-point))

			(if (> difference 0)
				(progn
					(incf num-of-fixes)
					;; διόρθωσε το λάθος σημάδι για το κάθετο κάτω κλαδί
					(setf node-to-fix (nth position-of-node (nth (+ level 1) *nodes-lower-branch-marks*)))
					(setf node-length (length node-to-fix))
					(if (not (null node-to-fix))
						(do ((index 0 (incf index)))
								((= index node-length))
							(setf (nth index node-to-fix) (+ (nth index node-to-fix) difference))
						)
					)
					;; διόρθωσε το λάθος σημάδι για τον κόμβο-αριθμό κάτω κλαδί
					(setf node-to-fix (nth position-of-node (nth (+ level 1) *nodes-number-marks*)))
					(setf node-length (length node-to-fix))
					(do ((index 0 (incf index)))
							((= index node-length))
						(setf (first (nth index node-to-fix)) (+ (first (nth index node-to-fix)) difference))
					)
					;; διόρθωσε το λάθος σημάδι για το κάθετο πάνω κλαδί
					(setf node-to-fix (nth position-of-node (nth (+ level 1) *nodes-upper-branch-marks*)))
					(setf node-length (length node-to-fix))
					(do ((index 0 (incf index)))
							((= index node-length))
						(setf (nth index node-to-fix) (+ (nth index node-to-fix) difference))
					)
					;; διόρθωσε το λάθος σημάδι για το οριζόντιο κλαδί (το που ξεκινάει, τα μήκη είναι υπολογισμένα σωστά εξ αρχής)
					(setf node-to-fix (nth position-of-node (nth (+ level 1) *nodes-horizontal-branch-marks*)))
					(setf (first node-to-fix) (+ (first node-to-fix) difference))
				)
			)
		)
	)
	(return-from fix-misplaced-nodes-and-branches num-of-fixes)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή υπολογίζει την απόσταση που πρέπει να αφήσουμε από την αρχή         ;;;
;;; (σημείο 0) μέχρι τον πρώτο κόμβο του κάθε επιπέδου. Αυτό που κάνει για να την        ;;;
;;; υπολογίσει είναι να προσθέτει κάθε φορα αθροιστικά την τιμή της προηγούμενης         ;;;
;;; υπολογισμένης απόστασης συν το μέσον του μήκους του πρώτου οριζόντιου κλαδιού.       ;;;
;;;**************************************************************************************;;;
(defun calculate-indent (previous-horizontal-branch-marks)
	(let ((current-indent nil))
		(if (null previous-horizontal-branch-marks)
			(setf current-indent 0)
			(let ((horizontal-branch-length (second (first previous-horizontal-branch-marks))))
				(if (oddp horizontal-branch-length)
					(setf current-indent (+ *previous-indent* (- (ceiling (/ horizontal-branch-length 2)) 1)))
					(setf current-indent (+ *previous-indent* (- (/ horizontal-branch-length 2) 1)))
				)
				(update-print-counter current-indent)
			)
		)
		(return-from calculate-indent current-indent)
	)
)

;;;**************************************************************************************;;;
;;; Συνάρτηση που ενημερώνει τον *print-counter* ο οποίος είναι ο counter που            ;;;
;;; χρησιμοποιούμε για να ορίζουμε όλα τα σημάδια.                                       ;;;
;;;**************************************************************************************;;;
(defun update-print-counter (n)
	(setf *print-counter* (+ *print-counter* n))
)

;;;**************************************************************************************;;;
;;; Συνάρτηση που υπολογίζει το μήκος ενός αριθμού, δηλαδή από πόσα ψηφία αποτελείται    ;;;
;;; αφού κάθε ψηφίο καταλαμβάνει έναν χαρακτήρα κατά την εκτύπωση.                       ;;;
;;;**************************************************************************************;;;
(defun get-number-length (number)
	(let ((num-of-digits 1) (divisor 1) (result nil) (flag nil))
		(loop
			(when (equal flag t) (return num-of-digits))
			(setf result (/ number divisor))
			(if (< result 10)
				(setf flag t)
				(progn (setf divisor (* divisor 10)) (incf num-of-digits))
			)
		)
	)
)

;;;**************************************************************************************;;;
;;; Συνάρτηση που υπολογίζει το μέσο του μήκους του οριζόντιου κλαδιού ενός κόμβου. Εάν  ;;;
;;; το μήκος του κλαδιού είναι περιττό τότε το μέσο υπολογίζεται ακριβώς στη μέση, π.χ.  ;;;
;;; μήκος = 3 => μέσο = 2. Εάν το μήκος του κλαδιού είναι άρτιο τότε το μέσο             ;;;
;;; υπολογίζεται (κατά σύμβαση) στην τιμή του αποτελέσματος της διαίρεσης του μήκους με  ;;;
;;; το 2, π.χ. για μήκος = 4 => μέσο = 2.                                                ;;;
;;;**************************************************************************************;;;
(defun get-middle-point-of-node-horizontal-branch (branch-length)
	(let ((middle-point (floor (/ branch-length 2))))
		(if (oddp branch-length)
			(setf middle-point (+ middle-point 1))
			middle-point
		)
	)
)

(defun push-nils-to-marks ()
	 (push nil *nodes-lower-branch-marks*)
	 (push nil *nodes-number-marks*)
	 (push nil *nodes-upper-branch-marks*)
	 (push nil *nodes-horizontal-branch-marks*)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή χρησιμοποιείται σε συνδυασμό με την πιο πάνω συνάρτηση              ;;;
;;; push-nils-to-marks για να μποροούμε να κάνουμε append κάθε φορά τα καινούργια        ;;;
;;; υπολογιμένα σημάδια, δηλαδή θέτουμε κάθε φορά τις τιμές nil στις λίστες με τα        ;;;
;;; αντίστοιχα σημάδια.                                                                  ;;;
;;;**************************************************************************************;;;
(defun append-marks ()
	(setf (first *nodes-lower-branch-marks*) (append (first *nodes-lower-branch-marks*) (list (pop *previous-level-nodes-lower-branch-marks*))))
	(setf (first *nodes-number-marks*) (append (first *nodes-number-marks*) (list (pop *previous-level-nodes-number-marks*))))
	(setf (first *nodes-upper-branch-marks*) (append (first *nodes-upper-branch-marks*) (list (pop *previous-level-nodes-upper-branch-marks*))))
	(setf (first *nodes-horizontal-branch-marks*) (append (first *nodes-horizontal-branch-marks*) (list (pop *previous-level-nodes-horizontal-branch-marks*))))
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή μετατρέπει τις λίστες που δεν είναι στην σωστή μορφή για εκτύπωση,  ;;;
;;; στην κατάλληλη μορφή, για να μπορουν να χρησιμοποιηθούν από τις συναρτήσεις που      ;;;
;;; εκτυπώνουν τα σημάδια.                                                               ;;;
;;;**************************************************************************************;;;
(defun convert-list-for-printing (list-with-marks)
	(dolist (level list-with-marks)
		(let ((temp nil))
			(dolist (elem level)
				(setf temp (append temp elem))
			)
			(setf (nth (position level list-with-marks) list-with-marks) temp)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση που εκτυπώνει το δέντρο στην οθόνη ή σε κάποιο αρχείο. Δέχεται το        ;;;
;;; όρισμα stream, το οπoίο όταν είναι t (true) τυπώνει στην οθόνη αλλιώς τυπώνει στο    ;;;
;;; αρχείο που έχει δώσει ο χρήστης. Το τύπωμα γίνεται γραμμή-γραμμή και επίπεδο-επίπεδο ;;;
;;; ξεκινώντας από την κορυφή προς το κατώτατο επίπεδο του δέντρου.                      ;;;
;;;**************************************************************************************;;;
(defun print-the-tree (stream)
	(dotimes (level (+ *tree-depth* 1))
		(if (> level 0)
			(progn
				(print-horizontal-branches (nth level *nodes-horizontal-branch-marks*) stream)
				(format stream "~%")
				(print-vertical-branches (nth level *nodes-upper-branch-marks*) stream)
			)
		)
		(format stream "~%")
		(print-node-numbers (nth level *nodes-number-marks*) stream)
		(format stream "~%")
		(if (< level *tree-depth*)
			(progn
				(print-vertical-branches (nth level *nodes-lower-branch-marks*) stream)
				(format stream "~%")
			)
		)
	)
	(format stream "~2%")
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή εκτυπώνει τους κόμβους-αριθμούς του δέντρου.                        ;;;
;;;**************************************************************************************;;;
(defun print-node-numbers (node-number-marks out)
	(let ((counter 0) (number-mark nil) (number nil))
		(loop
			(when (null node-number-marks) (return))
			(setf number-mark (first (first node-number-marks)))
			(setf number (second (first node-number-marks)))
			(if (= counter number-mark)
				(progn (format out "~S" number) (setf counter (+ counter (get-number-length number))) (pop node-number-marks))
			)
			(format out " ")
			(incf counter)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή εκτυπώνει τα κάθετα κλαδιά του δέντρου. Χρησιμοποιείται για την     ;;;
;;; εκτύπωση και των πάνω και των κάτω κλαδιών αφού και τα δύο αναπαριστώνται με το ίδιο ;;;
;;; σύμβολο ('|').                                                                       ;;;
;;;**************************************************************************************;;;
(defun print-vertical-branches (vertical-branch-marks out)
	(let ((counter 0))
		(loop
			(when (null vertical-branch-marks) (return))
			(if (= counter (first vertical-branch-marks))
				(progn (format out "|") (incf counter) (pop vertical-branch-marks))
			)
			(format out " ")
			(incf counter)
		)
	)
)

;;;**************************************************************************************;;;
;;; Η συνάρτηση αυτή εκτυπώνει τα οριζόντια κλαδιά του δέντρου.                          ;;;
;;;**************************************************************************************;;;
(defun print-horizontal-branches (horizontal-branch-marks out)
	(let ((counter 0) (start-of-horizontal-branch nil) (horizontal-branch-length nil))
		(loop
			(when (null horizontal-branch-marks) (return))
			(setf start-of-horizontal-branch (first (first horizontal-branch-marks)))
			(setf horizontal-branch-length (second (first horizontal-branch-marks)))
			(if (= counter start-of-horizontal-branch)
				(progn
					(dotimes (n horizontal-branch-length)
						(format out "-")
						(incf counter)
					)
					(pop horizontal-branch-marks)
				)
			)
			(format out " ")
			(incf counter)
		)
	)
)

(defun debugging (desc val)
	(format t "~&~A = ~S~%" desc val)
)
